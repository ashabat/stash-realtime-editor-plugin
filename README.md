# Stash Realtime Editor

The [Atlassian Stash](http://www.atlassian.com/software/stash/overview) Realtime Editor makes it possible to edit files in real-time directly from the Stash interface – no cloning, no IDE, no local editor. Because it’s real-time, you can share a link with your colleagues to collaboratively edit or review code together. Once you’re happy with your edits, you can commit them directly to Stash without the need to push your changes up to the repository. Your commit gets stored in a new branch and can be merged in with a simple pull request.

This add-on adds a real-time editor directly into Stash using the fabulous [Firebase](https://www.firebase.com/) service and it’s awesome [Firepad](http://www.firepad.io/) [operational transform](http://en.wikipedia.org/wiki/Operational_transform) (OT) based editor. OT makes it possible to edit files in real-time with reliability and predictability. With OT you can see what other users are doing inside the editor (i.e., selecting, highlighting, typing, etc.)

The Stash Realtime Editor works even if you lose network connectivity. If you’re working on a file with someone else and happen to suspend your laptop, resume your work on a train, and then come back online at home, your changes sync up as you would expect.

## Installation 

Install from within Stash using the Universal Plugin Manager (UPM), or download from the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.atlassian.stash.plugin.stash-editor-plugin).

After installation, you should see an **Edit** button in the toolbar of the **Source View** of text files stored in your repository. Clicking this will launch the real-time editor.

## Requirements

- Stash 2.12.0 or later (use [v1.0.5](https://bitbucket.org/atlassian/stash-realtime-editor-plugin/commits/tag/1.0.5) for Stash 2.0.0-2.12.x)
- an internet connection (the Stash server and any collaborating clients must be able to communicate with https://stasheditor.firebaseio.com)

The Stash Realtime Editor has been tested with the following browsers:

- Chrome 26
- Firefox 19
- Safari 6
- IE 9, 10

## Dude, where's my code? (a note on security)

The real-time editor uses a secured [Firebase](https://www.firebase.com/) instance to store temporary deltas while your code is being edited. Only users with the *Share URL* generated for each editing session are able to access those deltas.

Rest assured that your code is stored and retrieved in a secure manner. But if your company has restrictions on whether code fragments can temporarily exist in the cloud, the Stash Realtime Editor may not be for you.