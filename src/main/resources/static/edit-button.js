define('plugin/stash-edit-button', [
    'aui',
    'jquery',
    'underscore',
    'util/events',
    'exports'
], function(
    AJS,
    $,
    _,
    events,
    exports
) {

        function getParameterByName(name){
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + name + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(window.location.search);
            if(results == null)
                return "";
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function currentPath() {
            var url = location.pathname.match(/\/projects\/(.*)\/repos\/(.*)\/browse\/(.*)(?:$|\?)/);
            var at = getParameterByName("at");
            if (!at) {
                at = "HEAD";
            }

            return {
                pKey: url[1],
                repo: url[2],
                path: url[3],
                at: at
            };
        }

        function encodePathSegments(path) {
            return _.map(path.split("/"), function (component) {
                return encodeURIComponent(component);
            }).join('/');
        }

        exports.addEditButton = function(parentSelector) {
            var $editButton = $("<a class='aui-button'>Edit</a>");

            var appendButton = function() {
                $(parentSelector).append(
                    $editButton
                       .clone()
                       .click(function() {
                            var path = currentPath();
                            location.href = AJS.contextPath() + "/plugins/servlet/edit/projects/" +
                                path.pKey + "/repos/" + path.repo +
                                "/" + path.path + "?at=" + path.at;
                        })
                );
            };

            // no client-side web-items.. re-add the button whenever the source view is updated
            events.on("stash.feature.sourceview.dataLoaded", appendButton);
        }

    }
);

AJS.$(document).ready(function() {
    // .aui-toolbar2-primary was renamed to .primary in 2.5
    // .source-commands became .file-toolbar
    require('plugin/stash-edit-button').addEditButton(".file-toolbar .aui-toolbar2-primary, .file-toolbar .primary");
});