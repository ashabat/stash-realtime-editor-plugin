define('plugin/stash-editor', [
    'aui',
    'jquery',
    'underscore',
    'util/navbuilder',
    'model/page-state',
    'exports'
], function(
    AJS,
    $,
    _,
    navBuilder,
    pageState,
    exports
) {

    function bindPublish(target, codeMirror, onSubmit) {
        AJS.InlineDialog(AJS.$(target), 1, function(content, trigger, showPopup) {
            content.css({"padding":"10px"}).html(
                '<p>Commit message:</p>' +
                '<textarea id="commit-message" cols="39" rows="4">Online edit.</textarea>' +
                '<a class="aui-button" id="commit-edit" href="#">Create pull request...</a>'
            );
            showPopup();
            return false;
        }, {
            hideDelay: null
        });

        // ugh, should be using ajax instead of regular ole' POST form submission
        $("#commit-edit").live("click", function() {
            $("#editor-message").val($("#commit-message").val());
            $("#editor-content").val(codeMirror.getValue());
            $("#code-editor-form").submit();
            onSubmit();
        });

    }

    function initFirebase(sessionId, codeMirror, whenIPublish) {
        var session = new Firebase('https://stasheditor.firebaseio.com/' + sessionId);

        var firepad = Firepad.fromCodeMirror(session, codeMirror,{
            richTextShortcuts: false,
            richTextToolbar: false,
            userId: pageState.getCurrentUser().getName()
        });

        firepad.on('ready', function() {
            // if no history, initialise to original content of the file
            if (firepad.isHistoryEmpty()) {
                firepad.setText($("#original-content").val());
                codeMirror.scrollTo(0, 0);
            }
        });

        // display published dialog when another user publishes the document
        var published = session.child("published");
        var publishedCallback = published.on("value", function (published) {
            if (published.val()) {
                onPublished(published.val().user, published.val().branchLink);
            }
        });

        whenIPublish && whenIPublish.done(function() {
            // disable the published dialog popping up if we're the one publishing the change
            published.off("value", publishedCallback);
        });
    }

    function bindCollaborate(target, sessionId, codeMirror, whenIPublish) {
        initFirebase(sessionId, codeMirror, whenIPublish);

        AJS.InlineDialog(AJS.$(target), 1, function(content, trigger, showPopup) {
            var shareHref = location.href;
            if (shareHref.indexOf("?id=") === -1 && shareHref.indexOf("&id=") === -1) {
                shareHref += (location.href.indexOf("?") > -1 ? '&' : '?') + 'id=' + encodeURIComponent(sessionId);
            }
            content.css({"padding":"10px"}).html(
                '<p>Share this URL:</p>' +
                '<input class="quick-copy-text" type="text" size="55" value="' + shareHref + '">'
            );
            showPopup();
            return false;
        }, {
            hideDelay: null
        });
    }

    function onPublished(user, branchLink) {
        var dialog = new AJS.Dialog({width:400, height:200, id:"other-user-published", closeOnOutsideClick: true});
        dialog.addHeader("Published!");
        dialog.addPanel("SinglePanel", "<p><strong>" + user + "</strong> just published this file.</p>", "singlePanel");

        dialog.addButton("View Changes", function(dialog) {
            location.href = branchLink;
        });
        dialog.addButton("Keep Editing", function(dialog) {
            dialog.hide();
        });
        dialog.show();
    }

    /**
     * Start CodeMirror.
     */
    exports.onReady = function() {
        var sessionId = $("#session-id").val();
        var $originalContent = $("#original-content");
        var codeMirror = CodeMirror(document.getElementById('editor-wrapper'), {
            lineNumbers: true,
            lineWrapping: true,
            mode: $originalContent.data('content-type'),
            value : sessionId ? '' : $originalContent.val() // firebase handles this for is if we start a session.
        });
        codeMirror.focus();
        
        var whenIPublish;
        if ($("#publish[disabled]").size() === 0) {
            whenIPublish = bindPublish("#publish", codeMirror);
        }
        if (sessionId) { // collaboration is enabled
            bindCollaborate("#collaborate", sessionId, codeMirror, whenIPublish);
        }
    };

});
