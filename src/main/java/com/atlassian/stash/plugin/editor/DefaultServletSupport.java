package com.atlassian.stash.plugin.editor;

import com.atlassian.fugue.Either;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.exception.AuthorisationException;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryMetadataService;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.StashAuthenticationContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.isBlank;

public class DefaultServletSupport implements ServletSupport {

    private static final Pattern PATH_RX = Pattern.compile("^/projects/([^/]+)/repos/([^/]+)(/(.*)?)?$");
    private final StashAuthenticationContext authenticationContext;
    private final CommitService commitService;
    private final I18nService i18nService;
    private final LoginUriProvider loginUriProvider;
    private final RepositoryService repositoryService;
    private final RepositoryMetadataService repositoryMetadataService;

    public DefaultServletSupport(StashAuthenticationContext authenticationContext, CommitService commitService,
                                 I18nService i18nService, LoginUriProvider loginUriProvider,
                                 RepositoryService repositoryService, RepositoryMetadataService repositoryMetadataService) {
        this.authenticationContext = authenticationContext;
        this.commitService = commitService;
        this.i18nService = i18nService;
        this.loginUriProvider = loginUriProvider;
        this.repositoryService = repositoryService;
        this.repositoryMetadataService = repositoryMetadataService;
    }

    public Either<String, EntityDescriptor> resolveEntity(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        // Resolve the repository from the servlet path, flushing a friendly error message to the user if there are any
        // problems parsing the URI or if the repository is missing.
        Matcher m = PATH_RX.matcher(req.getPathInfo());
        if (!m.find()) {
            return Either.left(i18nService.getText("stash.repo.servlet.bad.path",
                    "That path could not be handled by the editor settings."));
        }

        Repository repository = null;
        try {
            repository = repositoryService.getBySlug(m.group(1), m.group(2));
        } catch (AuthorisationException e) {
            // ignore - we handle a missing/no_authz repository below in the same manner to not leak repository names
        }

        if (repository == null) {
            // Couldn't resolve the repository.. check if this is because the user isn't logged in (Stash didn't
            // support anonymous access at time of writing) or because the context user doesn't have the REPO_READ
            // permission.
            if (authenticationContext.getCurrentUser() == null) {
                // redirect to login
                StringBuffer originalUrl = req.getRequestURL();
                if (!isBlank(req.getQueryString())) {
                    originalUrl.append("?").append(req.getQueryString());
                }
                resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(originalUrl.toString())).toASCIIString());
                return Either.left("");
            } else {
                // render authz error message
                return Either.left(i18nService.getText("stash.repo.servlet.no.such.repository",
                        "The specified repository does not exist or you have insufficient permissions to access it."));
            }
        }

        // resolve path
        String path = m.group(4);
        if (path != null) {
            path = path.trim();
        }

        // resolve commit
        String at = req.getParameter("at");
        if (isBlank(at)) {
            // use default branch if unspecified
            at = repositoryMetadataService.getDefaultBranch(repository).getLatestChangeset();
        } else {
            // otherwise make sure we have the actual commit SHA, not just a ref
            at = commitService.getChangeset(repository, at).getId();
        }

        return Either.right((EntityDescriptor) new com.atlassian.stash.plugin.editor.EntityDescriptor(at, path, repository));
    }
}
